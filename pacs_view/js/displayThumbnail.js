function displayThumbnail(seriesList, seriesElement, element, stack,loaded) {
    // Deactivate other thumbnails
    $(seriesList).find('a').each(function() {
        $(this).removeClass('active');
    });

    // Make this series visible

    // Make the selected thumbnail active
    $(seriesElement).addClass('active');



    var enabledImage = jarvisframe.getEnabledElement(element);
    if (enabledImage.image) {
        // Stop clip from if playing on element
        jarvisframeTools.stopClip(element);
        // Disable stack scrolling
        jarvisframeTools.stackScroll.disable(element);
        // Enable stackScroll on selected series
        jarvisframeTools.stackScroll.enable(element);
    }

    // Load the first image of the selected series stack
    jarvisframe.loadAndCacheImage(stack.imageIds[0]).then(function(image) {
        if (loaded) {
            loaded.call(image, element, stack);
        }

        // Get the state of the stack tool
        var stackState = jarvisframeTools.getToolState(element, 'stack');
        stackState.data[0] = stack;
        stackState.data[0].currentImageIdIndex = 0;

        // Get the default viewport
        var defViewport = jarvisframe.getDefaultViewport(element, image);
        // Get the current series stack index
        // Display the image
        jarvisframe.displayImage(element, image, defViewport);




        //jarvis 加载位置信息
        var config = {
            drawAllMarkers: true
        }
        // Comment this out to draw only the top and left markers
        jarvisframeTools.orientationMarkers.setConfiguration(config);
        jarvisframeTools.orientationMarkers.enable(element);
        //end jarvis



        // Fit the image to the viewport window
        jarvisframe.fitToWindow(element);

        // Prefetch the remaining images in the stack (?)
        jarvisframeTools.stackPrefetch.enable(element);

        // Play clip if stack is a movie (has framerate)
        if (stack.frameRate !== undefined) {
            jarvisframeTools.playClip(element, stack.frameRate);
        }

    });
};