function setupViewport(element, stack, image) {
    // Display the image on the viewer element
    jarvisframe.displayImage(element, image);

    // If it's a movie (has frames), then play the clip
    if (stack.frameRate !== undefined) {
        jarvisframe.playClip(element, stack.frameRate);
    }

    // Activate mouse clicks, mouse wheel and touch
    jarvisframeTools.mouseInput.enable(element);
    jarvisframeTools.mouseWheelInput.enable(element);
    jarvisframeTools.touchInput.enable(element);

    // Enable all tools we want to use with this element
    jarvisframeTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
    jarvisframeTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
    jarvisframeTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
    jarvisframeTools.probe.enable(element);
    jarvisframeTools.length.enable(element);
    jarvisframeTools.ellipticalRoi.enable(element);
    jarvisframeTools.rectangleRoi.enable(element);
    jarvisframeTools.wwwcTouchDrag.activate(element);
    jarvisframeTools.zoomTouchPinch.activate(element);

    // Stack tools
    jarvisframeTools.addStackStateManager(element, ['playClip']);
    jarvisframeTools.addToolState(element, 'stack', stack);
    jarvisframeTools.stackScrollWheel.activate(element);
    jarvisframeTools.stackPrefetch.enable(element);


}
