function setupViewportOverlays(element, data) {
    var parent = $(element).parent();

    // Get the overlays
    var childDivs = $(parent).find('.overlay');
    var topLeft = $(childDivs[0]).find('div');
    var topRight = $(childDivs[1]).find('div');
    var bottomLeft = $(childDivs[2]).find('div');
    var bottomRight = $(childDivs[3]).find('div');

    // Set the overlay text
    //console.log(data);
    //todo 渲染信息
    if(data.patientName){
        $(topLeft[0]).text('姓名:' + data.patientName);
        $(topLeft[1]).text('ID:' + data.patientId);
        $(topRight[0]).text(data.studyDescription);
        $(topRight[1]).text(data.studyDate);
    }
    else
    {
        // $(topLeft[0]).text('姓名:' + data.PatientName);
        // $(topLeft[1]).text('ID:' + data.PatientID);
        // $(topLeft[2]).text('性别:'+ data.PatientSex);
        // $(topLeft[3]).text('年龄:' + data.PatientAge);
        // $(topRight[0]).text('检查描述:' + data.StudyDescription);
        // $(topRight[1]).text('检查日期:' + data.StudyDate);
        // $(bottomLeft[2]).text('SeriesNo #:' + data.SeriesNumber);

        $(topLeft[0]).text(data.PatientName);
        $(topLeft[1]).text(data.PatientID);
        $(topLeft[2]).text(data.PatientSex);
        $(topLeft[3]).text(data.PatientAge);
        $(topRight[0]).text(data.StudyDescription);
        $(topRight[1]).text(data.StudyDate);
        $(bottomLeft[2]).text(data.SeriesNumber);
    }



    // On new image (displayed?)
    function onNewImage(e, eventData) {
        // If we are currently playing a clip then update the FPS
        // Get the state of the 'playClip tool'
        var playClipToolData = jarvisframeTools.getToolState(element, 'playClip');

        // If playing a clip ...
        if (playClipToolData !== undefined && playClipToolData.data.length > 0 && playClipToolData.data[0].intervalId !== undefined && eventData.frameRate !== undefined) {

            // Update FPS
            $(bottomLeft[0]).text("FPS: " + Math.round(eventData.frameRate));
            //console.log('frameRate: ' + e.frameRate);

        } else {
            // Set FPS empty if not playing a clip
            if ($(bottomLeft[0]).text().length > 0) {
                $(bottomLeft[0]).text("");
            }
        }

        var toolData = jarvisframeTools.getToolState(element, 'stack');

        if(toolData === undefined || toolData.data === undefined || toolData.data.length === 0) {
            return;
        }
        var stack = toolData.data[0];


        $(bottomLeft[2]).text('SeriesNo #:' + stack.SeriesNumber);
        // Update Image number overlay
        $(bottomLeft[3]).text("Image # " + (stack.currentImageIdIndex + 1) + "/" + stack.imageIds.length);

        //获取MetaData
        var metadata = jarvisframeTools.metaData.get('imagePlane',stack.imageIds[stack.currentImageIdIndex])
        $(bottomLeft[4]).text(metadata.ImageOrientation);
    }
    // Add a jarvisframeNewImage event listener on the 'element' (viewer) (?)
    $(element).on("jarvisframeNewImage", onNewImage);


    // On image rendered
    function onImageRendered(e, eventData) {
        // Set zoom overlay text
        $(bottomRight[0]).text("Zoom:" + eventData.viewport.scale.toFixed(2));
        // Set WW/WL overlay text
        $(bottomRight[1]).text("WW/WL:" + Math.round(eventData.viewport.voi.windowWidth) + "/" + Math.round(eventData.viewport.voi.windowCenter));
        // Set render time overlay text
        $(bottomLeft[1]).text("Render Time:" + eventData.renderTimeInMs + " ms");


        $(bottomRight[2]).text("Rotation: " + Math.round(eventData.viewport.rotation) + "°");
    }

    // Add a jarvisframeImageRendered event listener on the 'element' (viewer) (?)
    $(element).on("jarvisframeImageRendered", onImageRendered);


}