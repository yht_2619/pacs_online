// Disable all tools
function disableAllTools(initmouse) {
    forEachViewport(function(element) {
        jarvisframeTools.wwwc.disable(element);
        jarvisframeTools.wwwcRegion.deactivate(element,1);
        jarvisframeTools.wwwcRegionTouch.deactivate(element);
        if (initmouse==1)
        {
            jarvisframeTools.wwwc.activate(element,1);   //左键初始化位ww/wl
        }
        jarvisframeTools.pan.activate(element, 2); // 2 is middle mouse button
        jarvisframeTools.zoom.activate(element, 4); // 4 is right mouse button

        jarvisframeTools.rotate.deactivate(element,1);
        jarvisframeTools.rotateTouchDrag.deactivate(element);
        jarvisframeTools.probe.deactivate(element, 1);
        jarvisframeTools.length.deactivate(element, 1);
        jarvisframeTools.angle.deactivate(element, 1);
        jarvisframeTools.ellipticalRoi.deactivate(element, 1);
        jarvisframeTools.rectangleRoi.deactivate(element, 1);
        jarvisframeTools.dragProbe.deactivate(element,1);
        jarvisframeTools.dragProbeTouch.deactivate(element);
        jarvisframeTools.stackScroll.deactivate(element, 1);
        jarvisframeTools.wwwcTouchDrag.deactivate(element);
        jarvisframeTools.zoomTouchDrag.deactivate(element);
        jarvisframeTools.panTouchDrag.deactivate(element);
        jarvisframeTools.stackScrollTouchDrag.deactivate(element);

        jarvisframeTools.highlight.deactivate(element,1);
        jarvisframeTools.highlight.disable(element);
        jarvisframeTools.probe.deactivate(element);
        jarvisframeTools.magnify.disable(element);
        jarvisframeTools.magnifyTouchDrag.disable(element);
        jarvisframeTools.magnify.deactivate(element);
        jarvisframeTools.magnifyTouchDrag.deactivate(element);

        jarvisframeTools.arrowAnnotate.deactivate(element,1);
        jarvisframeTools.arrowAnnotateTouch.deactivate(element);

        jarvisframeTools.lengthTouch.deactivate(element);
        jarvisframeTools.angleTouch.deactivate(element);
        jarvisframeTools.ellipticalRoiTouch.deactivate(element);
        jarvisframeTools.rectangleRoiTouch.deactivate(element);
        jarvisframeTools.dragProbeTouch.deactivate(element);
        jarvisframeTools.probeTouch.deactivate(element);
        jarvisframeTools.stackScrollTouchDrag.deactivate(element);
    });
}