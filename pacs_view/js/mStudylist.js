/**
 * Created by xiaobozhu on 2017/7/1.
 */
$(document).ready(function() {
    var myurl = GetHosturl();
    console.log(myurl);
    GetDataList();
    function GetDataList() {
        $.ajax({
            type: "POST",
            url: myurl +'test/Query/Study',
            cache: false, //禁用缓存
            dataType: "json",
            beforeSend: LoadFunction, //加载执行方法
            success: successGetData,
            error: errorMsg
        });
    }
    function LoadFunction() {
        console.log("正在加载");
    }
    function successGetData(result) {
        console.log(result);
        $("#studylist").html('');
        var json = eval(result); //数组
        $.each(json, function(idx, obj) {
            // var PatientName = "<h3 class='ui-li-heading'>" + obj.PatientName +"</h3>";
            // var PatientID = PatientName + "<p class='ui-li-desc>PatientID:<strong>" + obj.PatientID + "</strong></p>";
            // var PatientAge = PatientID + "<p class='ui-li-desc>PatientAge:<strong>" + obj.PatientAge + "</strong></p>";
            // var PatientSex = PatientAge + "<p class='ui-li-desc>PatientSex:<strong>" + obj.PatientSex + "</strong></p>";
            // var PatientSeries = PatientSex + "<span class='ui-li-count ui-body-inherit'>" + obj.Series.length + "</span>";
            // var Patient = "<li><a href='#' class='ui-btn ui-btn-icon-right ui-icon-carat-r' >" + PatientSeries + "</a></li>";
            // alert(Patient);
            var Patient ='<li><a href="#" class="ui-btn ui-btn-icon-right ui-icon-carat-r "><h3 class="ui-li-heading">'
                + obj.PatientName + '</h3><p class="ui-li-desc">PatientID:<strong>' + obj.PatientID
                + '</strong></p><p class="ui-li-desc">PatientAge:<strong>'
                + obj.PatientAge +'</strong></p> <p class="ui-li-desc">PatientSex:<strong>'
                + obj.PatientSex + '</strong></p><span class="ui-li-count ui-body-inherit">'
                + obj.Series.length + '</span></a></li>';
            $("#studylist").append(Patient)
        });
        $("#studylist").listview("refresh");
        // $.each(json, function (index, item) {
        //     //循环获取数据
        //     var name = json[index].Name;
        //     var idnumber = json[index].IdNumber;
        //     var sex = json[index].Sex;
        //     $("#studylist").html( "<br>" + name + " - " + idnumber + " - " + sex + "<br/>");
        // });
    }

    function errorMsg(result) {
        console.log(result);
    }
})