
function setupButtons(studyViewer) {
    // Get the button elements
    var buttons = $(studyViewer).find('button');
    var synchronizer = new jarvisframeTools.Synchronizer("jarvisframeNewImage", jarvisframeTools.updateImageSynchronizer);

    // for (var i=0;i<buttons.length;i++){
    //     $(buttons[i]).tooltip();
    // }
    $('.dropdown-menu li a').off('click touchstart');

    // selectAll
    $('.btnAllSelect').on('click touchstart', function() {
        $('div.active').find('.viewportWrapper').addClass("viewportWrapperSelect");
    });
    $('.btnref').off('click touchstart');
    $('.btnref').on('click touchstart',function () {
        var seriesindex = 0 ;
        // Get the stack data
        forEachViewportAll(function(element) {
            if (seriesindex == 0){
                var stackToolDataSource = jarvisframeTools.getToolState(element, 'stack');
                console.log('stackToolDataSource',stackToolDataSource);
                if (stackToolDataSource === undefined) {
                    return;
                }
                var stackData = stackToolDataSource.data[0];
                //console.log('stackData',stackData);
                jarvisframeTools.addStackStateManager(element, ['stack', 'referenceLines']);
                jarvisframeTools.addToolState(element, 'stack', stackData);
                synchronizer.add(element);

                // enable reference Lines tool
                jarvisframeTools.referenceLines.tool.enable(element, synchronizer);
                seriesindex ++;
            }
            else {
                synchronizer.add(element);

                // enable reference Lines tool
                jarvisframeTools.referenceLines.tool.enable(element, synchronizer);
            }

        });

    })
    // change ww/wl
    $('#menuwwwl li a').off('click touchstart');
    $('#menuwwwl li a').on('click touchstart',function(){
        if(this.id=='btnwwwlcustom')
        {
            BootstrapDialog.show({
                size:BootstrapDialog.SIZE_SMALL,
                type:BootstrapDialog.TYPE_DEFAULT,
                title: '请输入窗宽窗位',
                message: $('<div class="wwwwlDialogdiv" ><label for="wwTextInput">窗宽:</label><input id="inpww" name="wwTextInput"  type="text"/></div><div class="wwwwlDialogdiv"><label for="wlTextInput">窗位:</label><input id="inpwl" name="wlTextInput" type="text"/> </div>'),
                buttons: [ {
                    label: '确定',
                    // no title as it is optional
                    cssClass: 'btn-primary',
                    hotkey: 13,
                    action: function(dialogItself){
                        forEachViewport(function(element) {
                            changewwwl(element, $('#inpww').val(),$('#inpwl').val());
                        });
                        dialogItself.close();
                    }
                },  {
                    label: '取消',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        }
        else
        {
            var arr = this.innerHTML.split('(');
            if(arr[1])
            {
                var strtmp = arr[1].replace(')','');
                var wwwl=strtmp.split('/')
                if(wwwl[1])
                {
                    forEachViewport(function(element) {
                        changewwwl(element, wwwl[0],wwwl[1]);


                    });
                }

            }
        }




    });


    //change ww/wl
    function changewwwl(element,ww,wl) {
        var viewport = jarvisframe.getViewport(element);
        viewport.voi.windowWidth = parseFloat(ww);
        viewport.voi.windowCenter = parseFloat(wl);
        jarvisframe.setViewport(element, viewport);

    };

    // WW/WL
    $('.btnwwwl').off('click touchstart');
    $('.btnwwwl').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.wwwc.activate(element, 1);
            jarvisframeTools.wwwcTouchDrag.activate(element);
        });
    });

    $('.btnstackScroll').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.stackScroll.activate(element, 1);
            jarvisframeTools.stackScrollTouchDrag.activate(element);
        });
    });


    // RegionWW/WL
    $('.btnwwwcRegion').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.wwwcRegion.activate(element,1);
            jarvisframeTools.wwwcRegionTouch.activate(element);
        });
    });
    // highlight

    $('.btnhighlight').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.highlight.activate(element,1);

        });
    });
    // dehighlight
    $('.btndehighlight').on('click touchstart', function() {

        forEachViewport(function(element) {

            jarvisframeTools.highlight.deactivate(element, 1);
            jarvisframeTools.highlight.disable(element);


        });
    });


    // Invert
    $('.btnInvert').off('click touchstart');
    $('.btnInvert').on('touchstart', function() {
        disableAllTools(1);
        forEachViewport(function(element) {
            var viewport = jarvisframe.getViewport(element);
            // Toggle invert

            if (viewport.invert === true) {
                viewport.invert = false;
            } else {
                viewport.invert = true;
            }
            jarvisframe.setViewport(element, viewport);
        });
    });

    // Zoom
    $('.btnzoom').off('click touchstart');
    $('.btnzoom').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.zoom.activate(element, 5); // 5 is right mouse button and left mouse button
            jarvisframeTools.zoomTouchDrag.activate(element);
        });
    });



    // Magnif
    $('.btnMagnify1').on('click touchstart', function() {
        var config = {
            magnifySize: parseInt(225, 10),
            magnificationLevel: parseInt(2, 10)
        };
        jarvisframeTools.magnify.setConfiguration(config);
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.pan.activate(element, 2);
            jarvisframeTools.zoom.activate(element, 4);

            // Enable all tools we want to use with this element
            jarvisframeTools.magnify.activate(element, 1);
            jarvisframeTools.magnifyTouchDrag.activate(element);
        });
    });
    $('.btnMagnify2').on('click touchstart', function() {
        var config = {
            magnifySize: parseInt(225, 10),
            magnificationLevel: parseInt(3, 10)
        };
        jarvisframeTools.magnify.setConfiguration(config);
        disableAllTools();
        forEachViewport(function(element) {

            // Enable all tools we want to use with this element
            jarvisframeTools.magnify.activate(element, 1);
            jarvisframeTools.magnifyTouchDrag.activate(element);
        });
    });
    $('.btnMagnify3').on('click touchstart', function() {
        var config = {
            magnifySize: parseInt(225, 10),
            magnificationLevel: parseInt(4, 10)
        };
        jarvisframeTools.magnify.setConfiguration(config);
        disableAllTools();
        forEachViewport(function(element) {

            // Enable all tools we want to use with this element
            jarvisframeTools.magnify.activate(element, 1);
            jarvisframeTools.magnifyTouchDrag.activate(element);
        });
    });
    // Pan
    $('.btnPan').off('click touchstart');
    $('.btnPan').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.pan.activate(element, 3); // 3 is middle mouse button and left mouse button
            jarvisframeTools.panTouchDrag.activate(element);
        });
    });


    // orientation
    $('.btnRight').on('click touchstart', function() {
        //disableAllTools();
        forEachViewport(function(element) {
            viewport = jarvisframe.getViewport(element);
            viewport.rotation +=90;
            jarvisframe.setViewport(element, viewport);
        });
    });
    $('.btnLeft').on('click touchstart', function() {
        //disableAllTools();
        forEachViewport(function(element) {
            viewport = jarvisframe.getViewport(element);
            viewport.rotation -=90;
            jarvisframe.setViewport(element, viewport);
        });
    });
    //水平
    $('.btnhFlip').on('click touchstart', function() {
        //disableAllTools();
        forEachViewport(function(element) {
            viewport = jarvisframe.getViewport(element);
            viewport.hflip = !viewport.hflip;
            jarvisframe.setViewport(element, viewport);
        });
    });
    //垂直
    $('.btnvFlip').on('click touchstart', function() {
        //disableAllTools();
        forEachViewport(function(element) {
            viewport = jarvisframe.getViewport(element);
            viewport.vflip = !viewport.vflip;
            jarvisframe.setViewport(element, viewport);
        });
    });

    //resetRotate
    $('.btnResetRotate').on('click touchstart', function() {
        //disableAllTools();
        forEachViewport(function(element) {
            viewport = jarvisframe.getViewport(element);
            viewport.hflip = false;
            viewport.vflip = false;
            viewport.rotation = 0;
            jarvisframe.setViewport(element, viewport);
        });
    });

    //任意角度
    $('.btnRotate').off('click touchstart');
    $('.btnRotate').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.rotate.activate(element, 1);
            jarvisframeTools.rotateTouchDrag.activate(element);
        });
    });


    // Length measurement
    $('.btnLength').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.length.activate(element, 1);
            jarvisframeTools.lengthTouch.activate(element);
        });
    });

    // Angle measurement
    $('.btnAngle').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.angle.activate(element, 1);
            jarvisframeTools.angleTouch.activate(element);
        });
    });

    // Pixel probe
    $('.btnPixelprobe').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.probe.activate(element, 1);
            jarvisframeTools.probeTouch.activate(element);
        });
    });

    // Elliptical ROI
    $('.btnElliptical').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function(element) {
            jarvisframeTools.ellipticalRoi.activate(element, 1);
            jarvisframeTools.ellipticalRoiTouch.activate(element);
        });
    });

    // Rectangle ROI
    $('.btnRectangle').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function (element) {
            jarvisframeTools.rectangleRoi.activate(element, 1);
            jarvisframeTools.rectangleRoiTouch.activate(element);
        });
    });

    // drag probe
    $('.btnDragProbe').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function (element) {
            jarvisframeTools.dragProbe.strategy = jarvisframeTools.dragProbe.strategies.default;
            jarvisframeTools.dragProbe.activate(element, 1);
            jarvisframeTools.dragProbeTouch.strategy = jarvisframeTools.dragProbe.strategies.default;
            jarvisframeTools.dragProbeTouch.activate(element);
        });
    });

    // drag 标记
    var annotationDialog = document.querySelector('.annotationDialog');
    var relabelDialog = document.querySelector('.relabelDialog');
    dialogPolyfill.registerDialog(annotationDialog);
    dialogPolyfill.registerDialog(relabelDialog);
    function getTextCallback(doneChangingTextCallback) {
        var annotationDialog  = $('.annotationDialog');
        var getTextInput = annotationDialog .find('.annotationTextInput');
        var confirm = annotationDialog .find('.annotationDialogConfirm');

        annotationDialog .get(0).showModal();

        confirm.off('click');
        confirm.on('click', function() {
            closeHandler();
        });

        annotationDialog .off("keydown");
        annotationDialog .on('keydown', keyPressHandler);

        function keyPressHandler(e) {
            // If Enter is pressed, close the dialog
            if (e.which === 13) {
                closeHandler();
            }
        }

        function closeHandler() {
            annotationDialog .get(0).close();
            doneChangingTextCallback(getTextInput.val());
            // Reset the text value
            getTextInput.val("");
        }
    }

    // Define a callback to edit your text annotation
    // This could be used, e.g. to open a modal
    function changeTextCallback(data, eventData, doneChangingTextCallback) {
        var relabelDialog = $('.relabelDialog');
        var getTextInput = relabelDialog.find('.annotationTextInput');
        var confirm = relabelDialog.find('.relabelConfirm');
        var remove = relabelDialog.find('.relabelRemove');

        getTextInput.val(data.annotationText);
        relabelDialog.get(0).showModal();

        confirm.off('click');
        confirm.on('click', function() {
            relabelDialog.get(0).close();
            doneChangingTextCallback(data, getTextInput.val());
        });

        // If the remove button is clicked, delete this marker
        remove.off('click');
        remove.on('click', function() {
            relabelDialog.get(0).close();
            doneChangingTextCallback(data, undefined, true);
        });

        relabelDialog.off("keydown");
        relabelDialog.on('keydown', keyPressHandler);

        function keyPressHandler(e) {
            // If Enter is pressed, close the dialog
            if (e.which === 13) {
                closeHandler();
            }
        }

        function closeHandler() {
            relabelDialog.get(0).close();
            doneChangingTextCallback(data, getTextInput.val());
            // Reset the text value
            getTextInput.val("");
        }

    }
    var config = {
        getTextCallback : getTextCallback,
        changeTextCallback : changeTextCallback,
        drawHandles : false,
        drawHandlesOnHover : true,
        arrowFirst : true
    }
    $('.btnAnnotation').on('click touchstart', function() {
        disableAllTools();
        forEachViewport(function (element) {
            jarvisframeTools.arrowAnnotate.setConfiguration(config);

            // Enable all tools we want to use with this element
            jarvisframeTools.arrowAnnotate.activate(element, 1);
            jarvisframeTools.arrowAnnotateTouch.activate(element);
        });
    });

    $('.btnclearToolData').off('click touchstart');
    $('.btnclearToolData').on('click touchstart' , function() {
        forEachViewport(function (element) {
            // jarvisframeTools.clearToolState(element, "length");
            // jarvisframeTools.clearToolState(element, "probe");
            // jarvisframeTools.clearToolState(element, "rectangleRoi");
            // jarvisframeTools.clearToolState(element, "ellipticalRoi");
            // jarvisframeTools.clearToolState(element, "angle");
            // jarvisframeTools.clearToolState(element, "angle");
            var toolStateManager = jarvisframeTools.globalImageIdSpecificToolStateManager;
            // Note that this only works on ImageId-specific tool state managers (for now)
            toolStateManager.clear(element)

            jarvisframe.updateImage(element);

        });
    });

    // hui fu
    $('.btnhuifu').off('click touchstart');
    $('.btnhuifu').on('click touchstart', function() {
        disableAllTools(1);
        forEachViewport(function (element) {
            jarvisframe.reset(element);
            //jarvisframeTools.reset(element);
        });
    });

    // Play clip
    $('.btnPlay').off('click touchstart');
    $('.btnPlay').on('click touchstart', function() {

        forEachViewport(function(element) {
            var stackState = jarvisframeTools.getToolState(element, 'stack');
            var frameRate = stackState.data[0].frameRate;
            // Play at a default 10 FPS if the framerate is not specified
            if (frameRate === undefined) {
                frameRate = 10;
            }
            jarvisframeTools.playClip(element, frameRate);
        });
    });

    // Stop clip
    $('.btnStop').off('click touchstart');
    $('.btnStop').on('click touchstart', function() {
        forEachViewport(function(element) {
            jarvisframeTools.stopClip(element);
        });
    });
    // save
    $('.btnSave').off('click touchstart');
    $('.btnSave').on('click touchstart', function() {
        var savefilename='image';
        forEachViewport(function(element) {
            jarvisframeTools.saveAs(element, savefilename + '.png');
        });
    });


};