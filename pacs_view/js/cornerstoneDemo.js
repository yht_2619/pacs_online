// Load in HTML templates

var viewportTemplate; // the viewport template
// loadTemplate("templates/viewport.html", function(element) {
loadTemplate("templates/viewport_moblie.html", function(element) {

        viewportTemplate = element;
});

var studyViewerTemplate; // the study viewer template
loadTemplate("templates/studyViewer.html", function(element) {
    studyViewerTemplate = element;
});

// Get study list from JSON manifest
$.getJSON('studyList.json', function(data) {
  data.studyList.forEach(function(study) {

    // Create one table row for each study in the manifest
    var studyRow = '<tr><td>' +
    study.patientName + '</td><td>' +
    study.patientId + '</td><td>' +
    study.studyDate + '</td><td>' +
    study.modality + '</td><td>' +
    study.studyDescription + '</td><td>' +
    study.numImages + '</td><td>' +
    '</tr>';

    // Append the row to the study list
    var studyRowElement = $(studyRow).appendTo('#studyListData');

    /*$(function(){
      var item = {'id':'1','name':'首页','url':'son.html','closable':false};
      closableTab.addTab(item);
    })
    function add(){
      var id = $('#id').val();
      var name = $('#name').val();
      var uri = $('#url').val();
      var closable = $('#closable').val();
      var item = {'id':id,'name':name,'url':uri,'closable':closable==1?true:false};
      closableTab.addTab(item);
    }*/

    // On study list row click
    $(studyRowElement).click(function() {

      // Add new tab for this study and switch to it
     /* var studyTab = '<li><a href="#x' + study.patientId + '" data-toggle="tab" >' + study.patientName + '</a></li>';
      $('#tabs').append(studyTab);*/

      var id =study.patientId;
      var name = study.patientName;
      var uri = study.patientId;
      var closable = 1;
      var  item ={'id':id,'name':name,'url':uri,'closable':closable==1?true:false};
      closableTab.addTab(item);

      // Add tab content by making a copy of the studyViewerTemplate element
      var studyViewerCopy = studyViewerTemplate.clone();

      /*var viewportCopy = viewportTemplate.clone();
      studyViewerCopy.find('.imageViewer').append(viewportCopy);*/


      studyViewerCopy.attr("id", 'tab_container_' + study.patientId);
      // Make the viewer visible
      studyViewerCopy.removeClass('hidden');
      // Add section to the tab content
      studyViewerCopy.appendTo('#tabContent');

      // Show the new tab (which will be the last one since it was just added
      $('#tabs a:last').tab('show');

      // Toggle window resize (?)
      $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $(window).trigger('resize');
      });

      // Now load the study.json
      console.log('studyid:'+study.studyId);
      loadStudy(studyViewerCopy, viewportTemplate, study.studyId + ".json");
    });
  });
});


// Show tabs on click
$('#tabs a').click (function(e) {
  e.preventDefault();
  $(this).tab('show');
});

// Resize main
function resizeMain() {
  var height = $(window).height();
  $('#main').height(height - 50);
  $('#tabContent').height(height - 50 - 42);
}


// Call resize main on window resize
$(window).resize(function() {
    resizeMain();
});
resizeMain();


// Prevent scrolling on iOS
document.body.addEventListener('touchmove', function(e) {
  e.preventDefault();
});
