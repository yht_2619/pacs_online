$(document).ready(function() {
    // 图像页模版
    var viewportTemplate; // the viewport template
    loadTemplate("templates/viewport.html", function(element) {
        viewportTemplate = element;
    });

    var studyViewerTemplate; // the study viewer template
    loadTemplate("templates/studyViewer.html", function(element) {
        studyViewerTemplate = element;
    });
    //



    function  switab(tab,con,tab_c_css,tab_n_css,no) {
        $(tab).each(function(i){
            if(i == no)  		{
                $(this).addClass(tab_c_css);
            }else  		{
                $(this).removeClass(tab_c_css);
                $(this).addClass(tab_n_css);
            }
        })
        if (con)  	{
            $(con).each(function(i){
                if(i == no)      		{
                    $(this).show();
                }else      		{
                    $(this).hide();
                }
            })
        }
    }
    // 加载报告
    switab('#switab li','.content','on','',0);
    //
    $("#switab li").each(function(i){
        $(this).click(function(){
            switab('#switab li','.content','on','',i);
        })
    })

    $('#openStudy').on( 'click', function () {
        var studyViewerCopy = studyViewerTemplate.clone();

        studyViewerCopy.attr("id", 'tab_container_' + '591ec6145eeb185e89b1ab3b');
        // Make the viewer visible
        studyViewerCopy.removeClass('hidden');
        // Add section to the tab content
        $('#imgContent').html('');
        studyViewerCopy.appendTo('#imgContent');
        loadStudy(studyViewerCopy, viewportTemplate, '591ec6145eeb185e89b1ab3b');

    })

})