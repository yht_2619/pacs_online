var vue = new Vue({
    el: '#app',
    data: {
        options:[{
            placeholder:'医院Id',
            key:'hospitalNo',
            label:'hospitalNo'
        },{
            placeholder:'医院名称',
            key:'title',
            label:'title'
        },{
            placeholder:'报告名称',
            key:'subtitle',
            label:'subtitle'
        },{
            placeholder:'报告医生',
            key:'reportDoctor',
            label:'reportDoctor'
        },{
            placeholder:'报告时间',
            key:'reportDateTime',
            label:'reportDateTime'
        },{
            placeholder:'患者ID',
            key:'patientid',
            label:'patientid'
        },{
            placeholder:'姓名',
            key:'patientName',
            label:'patientName'
        },{
            placeholder:'性别',
            key:'patientSex',
            label:'patientSex'
        },{
            placeholder:'年龄',
            key:'patientAge',
            label:'patientAge'
        },{
            placeholder:'身份证',
            key:'idCrad',
            label:'idCrad'
        },{
            placeholder:'手机号',
            key:'phone',
            label:'phone'
        },{
            placeholder:'检查类别',
            key:'examClass',
            label:'examClass'
        },{
            placeholder:'检查项目',
            key:'examItem',
            label:'examItem'
        },{
            placeholder:'检查号',
            key:'examNo',
            label:'AccessionNumber'
        },{
            placeholder:'检查号',
            key:'studyid',
            label:'studyid'
        },{
            placeholder:'检查ID',
            key:'studyuid',
            label:'studyuid'
        },{
            placeholder:'检查时间',
            key:'examDateTime',
            label:'examDateTime'
        },{
            placeholder:'检查参数',
            key:'parameter',
            label:'parameter'
        },{
            placeholder:'所见',
            key:'impression',
            label:'impression'
        },{
            placeholder:'意见',
            key:'description',
            label:'description'
        }],model:{
            'hospitalNo':'',
            'title':'',
            'subtitle':'',
            'reportDoctor':'',
            'reportDateTime':'',
            'patientid':'',
            'patientName':'',
            'patientSex':'',
            'patientAge':'',
            'idCrad':'',
            'phone':'',
            'examClass':'',
            'examItem':'',
            'examNo':'',
            'studyid':'',
            'studyuid':'',
            'examDateTime':'',
            'parameter':'',
            'impression':'',
            'description':'',
        }
    },
    methods: {
        back() {
            window.location = './search.html'
        },addOpinion() {
            this.descriptions.push({content:''})
        },deleteOpinion(index){
            if(this.descriptions.length > 1) {
                this.descriptions.splice(index, 1)
            } else {
                alert('最少填写一条意见')
            }
        },back() {
            window.location = './search.html';
        },createReport() {
            var that = this;
            console.log(this.model)
            var model        = this.model;
            var data = {
                hospitalNo      :model.hospitalNo,
                title           :model.title,
                subtitle        :model.subtitle,
                reportDoctor    :model.reportDoctor,
                reportDateTime  :model.reportDateTime,
                patientid       :model.patientid,
                patientName     :model.patientName,
                patientSex      :model.patientSex,
                patientAge      :model.patientAge,
                idCrad          :model.idCrad,
                phone           :model.phone,
                examClass       :model.examClass,
                examItem        :model.examItem,
                examNo          :model.examNo,
                studyuid        :model.studyuid,
                examDateTime    :model.examDateTime,
                impression      :model.impression,
                description     :model.description,
                parameter       :model.parameter,
                studyid         :model.studyid
            }
            req(REPOET_QUERY_CREATE,data,function (res) {
                alert("报告提交完成");
                that.model = {
                    'hospitalNo':'',
                    'title':'',
                    'subtitle':'',
                    'reportDoctor':'',
                    'reportDateTime':'',
                    'patientid':'',
                    'patientName':'',
                    'patientSex':'',
                    'patientAge':'',
                    'idCrad':'',
                    'phone':'',
                    'examClass':'',
                    'examItem':'',
                    'examNo':'',
                    'studyid':'',
                    'studyuid':'',
                    'examDateTime':'',
                    'parameter':'',
                    'impression':'',
                    'description':'',
                }
                window.history.back()
            })
        }
    },
    created() {
    }
})