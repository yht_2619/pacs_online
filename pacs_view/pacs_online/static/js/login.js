var vue = new Vue({
    el: '#app',
    data: {
        placeholder:'身份证',
        username:'',
        password:'',
        type:1,
        codePrompt:'发送验证码',
        prompt:{
            usernamePrompt:'姓名',
            passwordPrompt:'身份证',
        },
        templatePrompt:{
            name:{
                usernamePrompt:'姓名',
                passwordPrompt:'身份证',
            },
            phone:{
                usernamePrompt:'手机号',
                passwordPrompt:'验证码',
            }
        }
    },
    methods: {
        check(){
            var type = this.type
            if(type == 1) {
                req(LOGIN_BY_PASSWORD,{username:this.username,password:this.password}, data => {
                        console.log(data)
                        setToken(data.token)
                        window.location = './report_list.html';
                })
            } else if(type == 2) {
                req(LOGIN_BY_CHECK_CODE,{phone:this.username,code:this.password},data => {
                        console.log(data)
                        setToken(data.token)
                        window.location = './report_list.html' ;

                })
            }

        },
        changeLoginByName() {
            if(this.type != 1) {
                this.type = 1
                this.placeholder = '身份证'
                this._clearData()
                this._changePrtompt();
            }

        },
        changeLoginByPhone() {
            if(this.type != 2) {
                this.type = 2
                this._clearData()
                this._changePrtompt();
            }

        },sendCode() {
            var that = this;
            if(that.codePrompt == '发送验证码') {
                req(LOGIN_SEND_CODE, {phone: this.username}, data => {
                    that.codePrompt = 60;
                    that._autoRun(1000, function () {
                        if (that.codePrompt == 0) {
                            that.codePrompt = '发送验证码';
                            return false;
                        } else {
                            that.codePrompt -= 1;
                            return true;
                        }
                    })
                    console.log(data)
                })

            }

        },
        _autoRun(time,func,count){
            var that = this;
            setTimeout(function () {
                if (func()) {
                    that._autoRun(time, func );
                }
            },time)
        },
        _clearData() {
            this.username = '';
            this.password = '';
        },_changePrtompt() {
            if(this.type == 1) {
                this.prompt = this.templatePrompt.name
                // this.placeholder = '身份证'
            } else if(this.type == 2) {
                this.prompt = this.templatePrompt.phone
                // this.placeholder = '验证码'
            }
        },back() {
            window.location = './more_action.html'
        }
    }
})
