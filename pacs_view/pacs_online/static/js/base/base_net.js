const defaultErrorHandler = function(error){
    if (error.msg){
        alert(error.msg);
    }else{
        alert(error);
    }
}
const req = function(url,data,successCallBack,errorCallBack){
    if (arguments.length < 2){
        defaultErrorHandler({msg:'请求参数错误'});
        return;
    }
    if (typeof arguments[arguments.length - 1] != "function"){
        defaultErrorHandler(page, {"code":-101, "msg":"最后一个参数必须是函数"})
        return;
    }
    console.log(url, data)
    $.post(url, data, function(result){
        if(result.code == 0) {
            successCallBack(result.data);
        } else {
            console.error(url, data,result)
            if (errorCallBack) {
                errorCallBack(result)
            } else {
                defaultErrorHandler(result)
            }
        }

    });
}
