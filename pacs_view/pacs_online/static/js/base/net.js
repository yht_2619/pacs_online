const searchFunc = {
    1 : function (param,cb) {
        console.log('REPOET_QUERY')
        req(REPOET_QUERY_AND_PATIENT,{patientId:param},data => {
            cb(data)
        })
    },
    2:function (param,cb) {
        req(REPOET_QUERY_AND_PATIENT,{studyUid :param},data => {
            cb(data)
        })
    },
    3:function (param,cb) {
        req(REPOET_QUERY_AND_PATIENT,{studyId:param},data => {
            cb(data)
        })
    },
    4:function (param,cb) {
        req(REPOET_QUERY_AND_PATIENT,{ seriesUid:param},data => {
            cb(data)
        })
    },
    5:function (param,cb) {
        req(REPOET_QUERY_AND_PATIENT,{imageId:param},data => {
            cb(data)
        })
    },
    8:function (param,cb) {
        req(REPOET_QUERY_BY_IDCRAD,{idCrad:param},data => {
            cb(data)
        })
    },
    9:function (param,cb) {
        req(REPOET_QUERY_BY_PHONE,{ phone:param},data => {
            cb(data)
        })
    },
    7:function (param,cb) {
        req(REPOET_QUERY_BY_EXAMNO,{examNo:param},data => {
            cb(data)
        })
    }
}