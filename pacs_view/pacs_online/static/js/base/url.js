// const BASE_URL = 'http://127.0.0.1:9999/';
// const BASE_URL = 'http://eye.zspcl.com:20001/';
// const BASE_URL = 'http://eye.zspcl.com:9999/';
const BASE_URL = 'http://139.199.119.129:9998/';

const BASE_REPOET_QUERY_URL = BASE_URL + 'report/';


const LOGIN_BY_PASSWORD = BASE_URL + 'account/' + 'loginByPassword'
const LOGIN_SEND_CODE = BASE_URL + 'account/' + 'sendValidationCode'
const LOGIN_BY_CHECK_CODE = BASE_URL + 'account/' + 'loginByCode'

const REPOET_QUERY_BY_TOKEN = BASE_URL + 'report/' + 'getByUserToken'
const REPORT_GET_STUDY_UID = BASE_URL + 'query/' + 'getStudyUid'//获取studyUid


const REPOET_QUERY = BASE_URL + 'query';//查询患者
const REPOET_QUERY_BY_IDCRAD = BASE_REPOET_QUERY_URL + 'getByIdcard';//通过身份证查报告
const REPOET_QUERY_BY_PHONE = BASE_REPOET_QUERY_URL + 'getByphone';//通过手机号查报告
const REPOET_QUERY_BY_EXAMNO= BASE_REPOET_QUERY_URL + 'getByExamNo';//通过检查号查报告
const REPOET_QUERY_CREATE = BASE_REPOET_QUERY_URL + 'create';//创建报告
const REPOET_QUERY_AND_PATIENT =  REPOET_QUERY + '/queryPatientAndReport'//查询患者和报告
// const REPORT_GET_STUDY_UID = REPOET_QUERY + '/getStudyUid'//获取studyUid

const ACCOUNT_BASE_URL = BASE_URL + 'account/'//获取studyUid
const ACCOUNT_LOGIN_BY_IDCRAD = ACCOUNT_BASE_URL + 'loginByIdcrad'//身份证登录
const ACCOUNT_LOGIN_BY_CHECK_CODE = ACCOUNT_BASE_URL + 'checkCode'//校验验证码
const ACCOUNT_LOGIN_BY_SEND_CODE = ACCOUNT_BASE_URL + 'sendCode'//发送证码
const ACCOUNT_LOGIN_ADD_ACOUNT = ACCOUNT_BASE_URL + 'create'//发送证码

const SEARCH_REPORT_BY_STUDY_UID = BASE_REPOET_QUERY_URL + 'getByStudyUid'//
const SEARCH_REPORT_BY_ACCESSION_BUMBER = BASE_REPOET_QUERY_URL + 'getByExamNo'//
const SEARCH_REPORT_BY_PATIENT_ID = BASE_REPOET_QUERY_URL + 'getByPatientId'//

const UPLOAD_DICOM = BASE_URL + 'upload';//上传DICOM图
