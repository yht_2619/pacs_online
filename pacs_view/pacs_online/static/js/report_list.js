var vue = new Vue({
    el: '#app',
    data: {
        reports:[],
        add_reports:[],
        token:''
    },
    methods: {
        addReport(){
            window.location = './search.html'
        },
        _getReports(){
            var that = this
            this.add_reports = getObjItem('add_reports_' + this.token);
            console.log('add_reports',this.add_reports)
            var that = this;
            req(REPOET_QUERY_BY_TOKEN,{token:that.token},data => {
                console.log(data)
                that._formatRopert(data)
            })
        },goView(report){
            var that = this
            var request = {
                patientName:report.name,
                patientId:report.pid,
                patientAge:report.age,
                accessionNumber:report.examNo,
                impression:report.impression,
                description:report.description,
                studyUid:report.id,
                modality:report.modality,
                patientSex: report.sex,
                studyDate:report.check_date,
                reportDoctor : report.reportDoctor,
                reportDateTime:report.reportDateTime,
                reportName:report.reportName,
                hospital:report.hospital,
                token:that.token,
                identity:2
            }
            req(REPORT_GET_STUDY_UID,request,data => {
                request.studyUid = data['studyUid']
                console.log('REPORT_GET_STUDY_UID',data)
                setItem('report_data',request)
                window.location = './report_view_ifame.html'//?data=' + encodeURI(JSON.stringify(data))
            },err => {
                request.studyUid = null
                console.log('REPORT_GET_STUDY_UID',err)
                setItem('report_data',request)
                window.location = './report_view_ifame.html'
                console.log(err)
            })
        },back_goLogin() {
            window.location = './login.html';
        },formatInfo(report){
            var age = report.age;
            var sex = report.sex;
            var modality  = report.modality;
            var content = ''
            if(age) {
                content += ('年龄：' + age + ' ');
            }
            if(sex == "M") {
                sex = '男'
            }
            if(sex == "W") {
                sex = '女'
            }
            content += ("\t性别：" + sex + " ")
            content += ("\t检查类别：" + modality)
            return content;
        },_formatRopert(data) {
            var datas = []
            var ids = [];
            var reports = data['reports'];

            if(this.add_reports) {
                datas.push(...this.add_reports)
            }
            reports.forEach(report => {
                ids.push(report.reportId)
                datas.push({
                    name:report.patientName,
                    sex:report.patientSex ,
                    hospital:report.title == null ? '未记录' : report.title,
                    modality:report.examClass,
                    id:report.studyuid,
                    pid:report.patientid,
                    check_date:report.examDateTime,
                    age:report.patientAge,
                    // img:'../static/img/img_demo.png',//'dicomweb://eye.zspcl.com:20000/' + serie["images"][0].metadata,
                    examNo:report.examNo,
                    impression:report.impression,
                    description:report.description,
                    reportDoctor : report.reportDoctor,
                    reportDateTime:report.reportDateTime,
                    reportName:report.subtitle
                })
            })
            this.reports = datas
            setItem('report_ids' + this.token,ids)
        }
    },
    created() {
        //获取报告列表
        this.token = getToken()
        this._getReports();
    }
})


