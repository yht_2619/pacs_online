var vue = new Vue({
    el: '#app',
    data: {
        search_param:'',
        examNo:'',
        patientId:'',
        patientName:'',
        accessionNumber:'',
        studyUid:'',
        options:[
            {value:1,text:'patientId',demo:'70016'},
            {value:2,text:'studyUid',demo:'19664'},
            {value:3,text:'studyId',demo:'1.2.840.113619.2.81.290.9161.19664.20081211.195554'},
            {value:4,text:'seriesUid',demo:'1.2.840.113619.2.81.290.9161.19664.2.20081211.195641'},
            {value:5,text:'imageId',demo:'1.2.840.113619.2.81.290.9161.19664.2.14.20081211.195735'},
            {value:7,text:'检查号',demo:''},
            {value:8,text:'身份证号',demo:''},
            {value:9,text:'手机号',demo:''},
        ],
        search_key:9,
        type:1
    },
    methods: {
        searchByAccessionNumber(){
            this.type = 1
        },
        searchByPatientId(){
            this.type = 2
        },
        onSearchKeyChange(){
            this.search_param = '';
        },
        formattSearchKey(){
            var search_key = this.search_key;
            var text = '';
            for(var i = 0 ; i< this.options.length ; i++) {
                if(search_key == this.options[i].value) {
                    return '请输入' + this.options[i].text;
                }
            }
        },
        search() {
            var that = this;
            var type = that.type;
            if(type == 1) {
               req(SEARCH_REPORT_BY_ACCESSION_BUMBER,{token:that.token,examNo:that.examNo,patientName:that.patientName},data => {
                   that._formattAndSetReport(data)
               })
           } else if(type == 2) {
               req(SEARCH_REPORT_BY_PATIENT_ID,{token:that.token,patientid:that.patientId,patientName:that.patientName},data => {
                   that._formattAndSetReport(data)
               })
           }
        },back() {
            window.history.back();
        },upload() {
            window.location = './upload.html'
        },report() {
            window.location = './report_upload.html'
        },add_account(){
            window.location = './add_account.html'
        },_initOptions() {
            var type = getParam('type');
            var username = getParam('username');
            if(type == 1) {
                this.search_key = 8;
            } else if(type == 2) {
                this.search_key = 9;
            }
            this.search_param = username;
        },_formattAndSetReport(data) {
            var reports = []
            var ids = []
            var datas = data['reports']
            var add__reports = getObjItem('add_reports_' + this.token);
            var report_ids = getObjItem('report_ids' + this.token);

            var add__report_ids = getObjItem('add_report_ids_' + this.token);

            if(!add__reports) {
                add__reports = []
            }
            if(!add__report_ids) {
                add__report_ids = []
            }
            if(!report_ids) {
                report_ids = []
            }
            if(datas && datas.length > 0) {
                console.log(datas)
                datas.forEach(report => {
                    if(add__report_ids.indexOf(report.reportId) == -1 && report_ids.indexOf(report.reportId) == -1) {
                        reports.push({
                            name: report.patientName,
                            sex: report.patientSex,
                            hospital: report.title == null ? '未记录' : report.hospitalName,
                            modality: report.examClass,
                            id: report.studyuid,
                            pid: report.patientid,
                            check_date: report.examDateTime,
                            age: report.patientAge,
                            // img:'../static/img/img_demo.png',//'dicomweb://eye.zspcl.com:20000/' + serie["images"][0].metadata,
                            examNo: report.examNo,
                            impression: report.impression,
                            description: report.description,
                            reportDoctor: report.reportDoctor,
                            reportDateTime: report.reportDateTime,
                        })
                        add__report_ids.push(report.reportId)
                    } else {
                        alert('已有该报告')
                    }
                })
            } else {
                alert('没有查到报告')
            }
            if(add__reports) {
                reports.push(...add__reports)
            }

            if(add__report_ids) {
                ids.push(...add__report_ids)
            }
            setItem('add_reports_' + this.token,reports)
            setItem('add_report_ids_' + this.token,ids)
            console.log('add_reports_' + this.token,reports)
            window.history.back();
        }
    },
    created() {
        this.token = getToken()
        this._initOptions();
    }
})