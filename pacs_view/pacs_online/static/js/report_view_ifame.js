
var vue = new Vue({
    el: '#app',
    data: {
        user:{},
        see:'',
        suggestion:'',
        showReportFlag:true,
        data:'',
        // flag:1//有图有报告 2没图有报告 3有图没报告
        hasImage:false
    },
    methods: {
        showImg() {
            if(this.hasImage) {
                this.showReportFlag = false;
                document.body.style.overflow='hidden'
            } else {
                alert('图像还没有上传,请联系相关人员上传图像')
            }
        },
        showReport() {
            this.showReportFlag = true;
            document.body.style.overflow='auto'
        },
        _getUserInfo(){
            var report =  getObjItem('report_data')//JSON.parse(decodeURI(getParam('data')));
            console.log(report)
            // if(this.flag == 3) {
            //     this.showReportFlag = false;
            // }
            var name            = report.patientName
            var sex             = report.patientSex
            var date            = report.studyDate
            var age             = report.patientAge
            var reportDoctor    = report.reportDoctor
            var reportDateTime  = report.reportDateTime
            var reportName      =report.reportName
            var hospital        =report.hospital
            var modality        =report.modality
            var patientId       =report.patientId
            var user = {
                name,sex,date,age,reportDoctor,reportDateTime,reportName,hospital,modality,patientId
            }
            this.user = user;
            this.see  = report.impression || ''
            this.suggestion = report.description || '';
            if(report.studyUid) {
                var token = getToken()
                var data = {
                    "_id":report.studyUid,
                    "token": token,
                    "identity":2
                }
                this.data = encodeURI(JSON.stringify(data));
                this.hasImage = true;
                console.log('data',this.data)
            }
        },formatMargin() {
            var line_see = Math.ceil(this.see.length / 20);
            var line_suggestion = Math.ceil(this.suggestion.length / 20);
            var line_space = 10 - line_see - line_suggestion;
            var space = 0;
            if(line_space > 0) {
                space = 17 / 8 * line_space;
            }
            return {
                'margin-top':space + 'rem'
            }
        }
        ,formatHeight() {
            var height = screen.height;
            return {
                'height':height + 'px'
            }
        }
    },
    created() {
        this._getUserInfo();
    }
})