const url = 'http://139.199.119.129:9999/report/create'
const data = {
    title:'测试医院',//医院名称
    subtitle:'测试报告',//报告名称
    reportDoctor:"测试医生",//报告医生
    reportDateTime:"20170803",//报告时间
    parameter:'测试参数',//检查参数
    impression:"测试所见",//影像所见
    description:"测试建议",//建议
    img:'http://139.199.119.129/pacs_online/pacs-html/html/pacs_view/pacs_online/static/img/img_demo.png',//缩略图
    hospitalNo:"121",//医院ID
    patientid:"70016",//患者ID patientid
    patientName:"张三",//中文姓名
    patientSex:"男",//中文性别
    patientAge:"59",//年龄
    examClass:"CT",//检查类别 modality
    examItem:"胸腔CT",//检查项目
    examNo:'',//检查号accessionNumber
    studyuid:'1.2.840.113619.2.81.290.9161.19664.20081211.195554',//pacs检查ID studyUid
    examDateTime:'20081211',//检查时间
    phone:"18212345678",//手机号
    idCrad:"3426011123456786",//身份证
}
console.log(url, data)
$.post(url, data, function(result){
    console.log(result['report'])
});