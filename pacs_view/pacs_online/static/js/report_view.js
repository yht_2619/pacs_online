
var vue = new Vue({
    el: '#app',
    data: {
        user:{},
        see:{},
        suggestions:[],
    },
    methods: {
        _getUserInfo(){
            var user = {
                name:'张**',sex:'女',date:'2017-05-01 01:02:03',age:'47岁'
            }
            var see = '左肺下叶见一类圆形空洞性病变，大小约5.5cm*4.5cm，其内见气液平面影，洞壁不整，临近肺内见片状模糊影，局部与胸膜关系密切，后者略增厚。双肺上叶及右肺下叶背段见数个结节影，界限清晰，密度较高，直径小于1.0cm。纵隔居中，其内见多个结节状、板块状钙化密度影。心脏及大血管明显异常征象，所示肋骨及其余骨性胸廓明显异常'
            var suggestions = ['左肺下叶脓肿可能，建议抗炎随诊除外周围型肺癌','双肺及纵膈多发陈旧结核灶']
            this.user = user;
            this.see = see;
            this.suggestions = suggestions;
        }
    },
    created() {
        this._getUserInfo();
    }
})