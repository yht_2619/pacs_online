!function(e) {
    "use strict";
    e(window).on("load",
    function() {
        e(".preloader-wrap").fadeOut("slow",
        function() {
            e(this).remove()
        })
    }),
    e(".embed-responsive iframe").addClass("embed-responsive-item"),
    e(".carousel-inner .item:first-child").addClass("active"),
    e('[data-toggle="tooltip"]').tooltip(),
    e(".smoothscroll a.scroll-top").on("click",
    function(o) {
        o.preventDefault();
        var a = this.hash;
        e("html, body").stop().animate({
            scrollTop: e(a).offset().top - 50
        },
        1200)
    }),
    e(window).scroll(function() {
        e(this).scrollTop() > 800 ? e(".scroll-top:not(.scroll-wechat)").css("display", "block") : e(".scroll-top:not(.scroll-wechat)").css("display", "none")
    }),
    e(".navbar-nav li a").on("click",
    function(o) {
        e(".navbar-toggle").removeClass("open"),
        e(".navbar-collapse").removeClass("collapse in").addClass("collapse").removeClass("open")
    }),
    e(".dropdown-toggle").on("click",
    function(o) {
        e(".navbar-toggle").addClass("open"),
        e(".navbar-collapse").addClass("collapse in").removeClass("collapse")
    }),
    e(".navbar-toggle").on("click",
    function() {
        e(this).toggleClass("open"),
        e("#scroll-menu").toggleClass("sticky-bottom")
    }),
    e(".scroll-wechat").mouseover(function() {
        e(this).find("div").css("display", "block")
    }),
    e(".scroll-wechat").mouseout(function() {
        e(this).find("div").css("display", "none")
    }),
    e("[name=username],[name=password]").keydown(function(o) {
        13 == o.keyCode && e("#loginBtn").click()
    });
    var o = "http://www.huiyihuiying.com";
    e("#loginBtn").click(function() {
        var o = {
            username: e("[name=username]").val(),
            password: e("[name=password]").val()
        };
        if ("" == o.username || "" == o.password) return void e(this).prev(".err0").removeClass("hidden").addClass("text-danger");
        e(this).prev(".err0").addClass("hidden");
        a({result:1});
    });
    var a = function(a) {
        var s = a.result;
        if (0 == s) if (a.message) {
            var t = JSON.parse(a.message)[0];
            localStorage.setItem("userid", t.userid),
            localStorage.setItem("password", t.password),
            localStorage.setItem("user", JSON.stringify(t));
            var l = t.usertype,
            i = t.status;
            if (0 == l || 1 == l) {
                var n = localStorage.getItem("imageID"),
                r = localStorage.getItem("checkCode");
                window.location.href = null == n || "" === n || "" === r ? o + "/idoctor/html/home.html#/app/main/": o + "/idoctor/html/home.html#/app/datacenter/imageadd?imageid=" + n + "&checkCode=" + r
            } else if (4 == i) {
                var n = localStorage.getItem("imageID"),
                r = localStorage.getItem("checkCode");
                window.location.href = null == n || "" === n || "" === r ? o + "/idoctor/html/home.html#/app/main/": o + "/idoctor/html/home.html#/app/datacenter/imageadd?imageid=" + n + "&checkCode=" + r
            } else 2 == l ? window.location.href = o + "/idoctor/html/home.html#/app/main/": 3 == l && (window.location.href = o + "/idoctor/html/home.html#/app/main/")
        } else e("#msg").html("解析数据出错");
        else if (1 == s) e("#msg").html("用户名尚未注册"),
        console.log(this),
        e(".err1").removeClass("hidden").addClass("text-danger");
        else if (2 == s) e("#msg").html("密码错误"),
        e(".err2").removeClass("hidden").addClass("text-danger");
        else if ( - 1 == s) e("#msg").html("登陆失败"),
        e(".err3").removeClass("hidden").addClass("text-danger");
        else {
            if (void 0 == s) return void alert("该浏览器不支持系统，请下载UC浏览器!");
            e("#msg").html("服务器异常")
        }
    },
    s = function(e) {}
} (jQuery);